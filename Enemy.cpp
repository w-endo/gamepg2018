#include "Enemy.h"
using namespace MyGameEngine;


Enemy::Enemy()
{
	_position = D3DXVECTOR3((float)(rand()%210-100)/10.0f, 0, 20);
}


Enemy::~Enemy()
{
}

HRESULT Enemy::Init()
{
	return Fbx::Init("enemy.fbx");
}

BOOL Enemy::Update()
{
	_position.z -= 0.05f;
	return FALSE;
}
