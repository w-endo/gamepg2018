﻿//////////////////////////////////////////////////
//　カメラを管理するクラス
//　制作日：2018/6/5
//////////////////////////////////////////////////
#pragma once
#include "Node.h"

namespace MyGameEngine
{
	class Camera: public Node
	{
		//ビュー行列作成に必要な情報
		D3DXVECTOR3 _target;	//注視点
		D3DXVECTOR3 _up;		//上方向

		//プロジェクション行列作成に必要な情報
		float _angle;		//画角
		float _aspect;		//アスペクト比
		float _nearClip;	//近クリッピング面
		float _farClip;		//遠クリッピング面

		//座標変換行列
		D3DXMATRIX _view;	//ビュー行列
		D3DXMATRIX _proj;	//プロジェクション行列

	public:
		//コンストラクタ
		Camera();

		//デストラクタ
		~Camera();

		//機能：更新処理
		//引数：なし
		//戻値：シーン切り替えたらTRUE
		BOOL Update();

		void Draw() {}


		//以下セッター
		void SetPosition(D3DXVECTOR3 position);
		void SetTarget(D3DXVECTOR3 target);
		void SetUp(D3DXVECTOR3 up);

		//以下ゲッター
		D3DXVECTOR3 GetPosition();
		D3DXVECTOR3 GetTarget();
		D3DXVECTOR3 GetUp();
	};
}
