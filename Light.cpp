﻿#include "Light.h"

using namespace MyGameEngine;

//コンストラクタ
Light::Light()
{
	//ライト専用構造体
	D3DLIGHT9 lightState;

	//まずはクリア
	ZeroMemory(&lightState, sizeof(lightState));

	//ライトの種類
	lightState.Type = D3DLIGHT_DIRECTIONAL;		//平行光源

	//ライトの向き
	lightState.Direction = D3DXVECTOR3(1, -1, 1);

	//ライト色
	lightState.Diffuse.r = 1.0f;
	lightState.Diffuse.g = 1.0f;
	lightState.Diffuse.b = 1.0f;

	//環境光の色
	lightState.Ambient.r = 1.0f;
	lightState.Ambient.g = 1.0f;
	lightState.Ambient.b = 1.0f;

	//上記の設定でライトを作る
	g.pDevice->SetLight(0, &lightState);

	//点灯
	g.pDevice->LightEnable(0, TRUE);
}

//デストラクタ
Light::~Light()
{
}