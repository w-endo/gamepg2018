﻿#include "TitleScene.h"
#include "Camera.h"
#include "Light.h"
#include "TitleLogo.h"

TitleScene::TitleScene()
{
}


TitleScene::~TitleScene()
{
}

HRESULT TitleScene::Init()
{
	_pNodes.push_back(new MyGameEngine::Light);
	_pNodes.push_back(new MyGameEngine::Camera);
	_pNodes.push_back(new TitleLogo);

	return S_OK;
}
