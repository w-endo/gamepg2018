﻿#include "Scene.h"

using namespace MyGameEngine;

Scene::Scene()
{
}


Scene::~Scene()
{
	for (int i = 0; i < _pNodes.size(); i++)
	{
		SAFE_DELETE(_pNodes[i]);
	}
}

HRESULT Scene::InitEach()
{
	for (int i = 0; i < _pNodes.size(); i++)
	{
		if (FAILED(_pNodes[i]->Init()))
		{
			return E_FAIL;
		}
	}
	return S_OK;
}

void Scene::UpdateEach()
{
	for (int i = 0; i < _pNodes.size(); i++)
	{
		if (_pNodes[i]->Update())
		{
			return;
		}
	}

	for (auto it = _pNodes.begin(); it != _pNodes.end();)
	{
		if ((*it)->IsDead() == TRUE)
		{
			SAFE_DELETE(*it);
			it = _pNodes.erase(it);
		}
		else
		{
			it++;
		}
	}

	for (int i = 0; i < _pNodes.size(); i++)
	{
		for (int j = 0; j < _pNodes.size(); j++)
		{
			if (i == j)	continue;
			_pNodes[i]->Hit((Fbx*)_pNodes[j]);
		}
	}



}

void Scene::Draw()
{
	for (int i = 0; i < _pNodes.size(); i++)
	{
		_pNodes[i]->Draw();
	}
}

void Scene::AddChild(Node * node)
{
	node->SetParent(this);
	_pNodes.push_back(node);
}

