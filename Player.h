﻿#pragma once
#include "Fbx.h"

class Bullet;
class Player : public  MyGameEngine::Fbx
{
	Bullet* _pBaseBullet;
public:
	Player();
	~Player();
	HRESULT Init() override;
	BOOL Update() override;
	void Hit(Fbx* pTarget) override;
};

