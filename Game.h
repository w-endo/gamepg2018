﻿//////////////////////////////////////////////////
//　ゲーム全体を管理するクラス
//　制作日：2018/5/24
//////////////////////////////////////////////////
#pragma once

//インクルード
#include <vector>
#include "Global.h"
#include "Node.h"

class Game
{
	//Direct3Dオブジェクト
	LPDIRECT3D9	_pD3d;	   

	//ゲームに登場する各オブジェクト





	/*****　ここからprivate関数　*****/
	//機能：Direct3Dの準備
	//引数：hWnd　ウィンドウハンドル
	//戻値：なし
	void InitDirect3D(HWND hWnd);

	//機能：レンダリングの設定
	//引数：なし
	//戻値：なし
	void RenderPreference();
	

public:
	//コンストラクタ
	Game();	       

	//デストラクタ
	~Game();	  

	//機能：初期化処理
	//引数：hWnd　ウィンドウハンドル
	//戻値：なし
	HRESULT Init(HWND hWnd);	  

	//機能：更新処理
	//引数：なし
	//戻値：なし
	void Update();

	//機能：描画処理
	//引数：なし
	//戻値：なし
	void Draw();
};