﻿//////////////////////////////////////////////////
//　すべてのソースで必要となる情報をまとめた
//　制作日：2018/5/24
//////////////////////////////////////////////////
#pragma once

//インクルード
#include <Windows.h>
#include <d3dx9.h>
#include "Input.h"


//安全開放をするためのマクロ
#define SAFE_DELETE(p) if(p != NULL){ delete p; p = NULL;}
#define SAFE_RELEASE(p) if(p != NULL){ p->Release(); p = NULL;}
#define SAFE_DELETE_ARRAY(p) if(p != NULL){ delete[] p; p = NULL;}

namespace MyGameEngine
{
	class Scene;
}


class Global
{

public:
	//定数
	const int	WINDOW_WIDTH = 800;		//ウィンドウの幅
	const int	WINDOW_HEIGHT = 600;	//ウィンドウの高さ
	LPDIRECT3DDEVICE9	pDevice;	    //Direct3Dデバイスオブジェクト
	MyGameEngine::Input*	pInput;
	MyGameEngine::Scene*	pScene;

	//関数
	void ChangeScene(MyGameEngine::Scene* newScene);
};

//グローバルオブジェクト（外部宣言）
extern Global g;