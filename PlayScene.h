#pragma once
#include "Scene.h"

class Player;
class Enemy;

class PlayScene : public MyGameEngine::Scene
{
public:
	PlayScene();
	~PlayScene();
	HRESULT Init() override;
};
