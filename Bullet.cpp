﻿#include "Bullet.h"
#include "Enemy.h"
#include <typeinfo>

using namespace MyGameEngine;


Bullet::Bullet()
{
}


Bullet::~Bullet()
{
}

HRESULT Bullet::Init()
{
	return Fbx::Init("bullet.fbx");
}

BOOL Bullet::Update()
{
	_position.z += 0.2f;

	if (_position.z > 30)
	{
		_isDead = TRUE;
	}

	return FALSE;
}

void Bullet::Hit(Fbx * pTarget)
{
	if (typeid(*pTarget) == typeid(Enemy))
	{
		//自分からpTargetに向かうベクトル
		D3DXVECTOR3 v = pTarget->GetPosition() - this->GetPosition();

		//そのベクトルの長さを測る
		float length = D3DXVec3Length(&v);

		if(length < 1)
		{
			_isDead = TRUE;
			pTarget->Kill(TRUE);

		}
	}
}