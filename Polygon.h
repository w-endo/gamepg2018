﻿//////////////////////////////////////////////////
//　四角ポリゴンを表示する
//　制作日：2018/5/29
//////////////////////////////////////////////////
#pragma once
#include "Node.h"

namespace MyGameEngine	
{
	class Polygon : public Node
	{
		//ポリゴンの1頂点に必要なデータをまとめた構造体
		struct Vertex
		{
			D3DXVECTOR3 pos;	//位置
			D3DXVECTOR3 normal;	//法線
			DWORD       color;	//色
			D3DXVECTOR2 uv;		//UV座標
		};


		LPDIRECT3DVERTEXBUFFER9 _pVertexBuffer;	//頂点バッファ
		LPDIRECT3DINDEXBUFFER9	_pIndexBuffer;	//インデックスバッファ
		LPDIRECT3DTEXTURE9		_pTexture;		//テクスチャ
		D3DMATERIAL9			_material;		//マテリアル



		/*****　ここからprivate関数　*****/

		//機能：頂点情報の準備
		//引数：なし
		//戻値：なし
		void SetupVertex();

		//機能：インデックス情報の準備
		//引数：なし
		//戻値：なし
		void SetupIndex();

		//機能：見た目の準備
		//引数：テクスチャのファイル名
		//戻値：なし
		HRESULT SetupMaterial(LPCSTR fileName);



	public:
		//コンストラクタ
		Polygon();

		//デストラクタ
		~Polygon();

		//機能：初期化
		//引数：なし
		//戻値：成功したか
		HRESULT Init(LPCSTR fileName);

		//機能：描画処理
		//引数：なし
		//戻値：なし
		void Draw();


		//各セッター
		void SetPosition(D3DXVECTOR3 position);
		void SetPosition(float x, float y, float z);
		void SetRotate(D3DXVECTOR3 rotate);
		void SetRotate(float x, float y, float z);
		void SetScale(D3DXVECTOR3 scale);
		void SetScale(float x, float y, float z);

		//各ゲッター
		D3DXVECTOR3 GetPosition();
		D3DXVECTOR3 GetRotate();
		D3DXVECTOR3 GetScale();
	};
}