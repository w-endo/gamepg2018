﻿#include "Fbx.h"
using namespace MyGameEngine;

//コンストラクタ
Fbx::Fbx()
{
	_pManager = NULL;
	_pImporter = NULL;
	_pScene = NULL;

	_pVertexBuffer = NULL;
	_pIndexBuffer = NULL;
	_pTexture = NULL;


	_material = NULL;
}

//デストラクタ
Fbx::~Fbx()
{
	SAFE_DELETE_ARRAY(_polygonCountOfMaterial);
	SAFE_DELETE_ARRAY(_material);
	
	for (int i = 0; i < _materialCount; i++)
	{
		SAFE_RELEASE(_pTexture[i]);
		SAFE_RELEASE(_pIndexBuffer[i]);
	
	}
	SAFE_DELETE_ARRAY(_pTexture);
	SAFE_DELETE_ARRAY(_pIndexBuffer);
	SAFE_RELEASE(_pVertexBuffer);
	_pScene->Destroy();
	_pManager->Destroy();
}

//初期化
HRESULT Fbx::Init(LPCSTR fileName)
{
	_pManager = FbxManager::Create();
	_pImporter = FbxImporter::Create(_pManager, "");
	_pScene = FbxScene::Create(_pManager, "");

	_pImporter->Initialize(fileName);
	_pImporter->Import(_pScene);
	_pImporter->Destroy();

	char defaultCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);

	char dir[MAX_PATH];
	_splitpath_s(fileName, NULL, 0, dir, MAX_PATH, NULL, 0, NULL, 0);
	SetCurrentDirectory(dir);

	FbxNode* rootNode = _pScene->GetRootNode();
	int childCount = rootNode->GetChildCount();
	for (int i = 0; childCount > i; i++)
	{
		//ノードの内容をチェック
		CheckNode(rootNode->GetChild(i));
	}
	SetCurrentDirectory(defaultCurrentDir);

	return S_OK;

}

void Fbx::CheckNode(FbxNode* pNode)
{
	FbxNodeAttribute* attr = pNode->GetNodeAttribute();
	if (attr->GetAttributeType() == FbxNodeAttribute::eMesh)
	{
		//メッシュノードだった
		_materialCount = pNode->GetMaterialCount();
		_material = new D3DMATERIAL9[_materialCount];
		_pTexture = new LPDIRECT3DTEXTURE9[_materialCount];

		for (int i = 0; i < _materialCount; i++)
		{
			FbxSurfaceLambert* lambert = (FbxSurfaceLambert*)pNode->GetMaterial(i);
			FbxDouble3 diffuse = lambert->Diffuse;
			FbxDouble3 ambient = lambert->Ambient;
			ZeroMemory(&_material[i], sizeof(D3DMATERIAL9));

			_material[i].Diffuse.r = (float)diffuse[0];
			_material[i].Diffuse.g = (float)diffuse[1];
			_material[i].Diffuse.b = (float)diffuse[2];
			_material[i].Diffuse.a = 1.0f;

			_material[i].Ambient.r = (float)ambient[0];
			_material[i].Ambient.g = (float)ambient[1];
			_material[i].Ambient.b = (float)ambient[2];
			_material[i].Ambient.a = 1.0f;

			FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sDiffuse);
			FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);

			if (textureFile == NULL)
			{
				_pTexture[i] = NULL;
			}
			else
			{
				const char* textureFileName = textureFile->GetFileName();

				char name[_MAX_FNAME];
				char ext[_MAX_EXT];
				_splitpath_s(textureFileName, NULL, 0, NULL, 0, name, _MAX_FNAME, ext, _MAX_EXT);
				wsprintf(name, "%s%s", name, ext);
				D3DXCreateTextureFromFileEx(g.pDevice, name, 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
					D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &_pTexture[i]);
			}
		}

		CheckMesh(pNode->GetMesh());
	}
	else
	{
		//メッシュ以外のデータだった
		int childCount = pNode->GetChildCount();
		for (int i = 0; childCount > i; i++)
		{
			CheckNode(pNode->GetChild(i));
		}
	}
}

void Fbx::CheckMesh(FbxMesh* pMesh)
{
	FbxVector4* pVertexPos = pMesh->GetControlPoints();
	_vertexCount = pMesh->GetControlPointsCount();
	Vertex* vertexList = new Vertex[_vertexCount];
	_polygonCount = pMesh->GetPolygonCount();
	_indexCount = pMesh->GetPolygonVertexCount();

	for (int i = 0; _vertexCount > i; i++)
	{
		vertexList[i].pos.x = (float)pVertexPos[i][0];
		vertexList[i].pos.y = (float)pVertexPos[i][1];
		vertexList[i].pos.z = (float)pVertexPos[i][2];
	}

	for (int i = 0; i < _polygonCount; i++)
	{
		int startIndex = pMesh->GetPolygonVertexIndex(i);
		for (int j = 0; j < 3; j++)
		{
			int index = pMesh->GetPolygonVertices()[startIndex + j];
			FbxVector4 Normal;
			pMesh->GetPolygonVertexNormal(i, j, Normal);
			vertexList[index].normal =
				D3DXVECTOR3((float)Normal[0], (float)Normal[1], (float)Normal[2]);

			FbxVector2 uv = pMesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);
			vertexList[index].uv = D3DXVECTOR2((float)uv.mData[0], (float)(1.0 - uv.mData[1]));
		}
	}
	g.pDevice->CreateVertexBuffer(sizeof(Vertex) *_vertexCount, 0,
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED,
		&_pVertexBuffer, 0);

	Vertex *vCopy;
	_pVertexBuffer->Lock(0, 0, (void**)&vCopy, 0);
	memcpy(vCopy, vertexList, sizeof(Vertex) *_vertexCount);
	_pVertexBuffer->Unlock();

	delete[] vertexList;


	_pIndexBuffer = new IDirect3DIndexBuffer9*[_materialCount];
	_polygonCountOfMaterial = new int[_materialCount];

	for (int i = 0; i < _materialCount; i++)
	{
		int* indexList = new int[_indexCount];
		int count = 0;
		for (int polygon = 0; polygon < _polygonCount; polygon++)
		{
			int materialID = pMesh->GetLayer(0)->GetMaterials()->GetIndexArray().GetAt(polygon);
			if (materialID == i)
			{
				for (int vertex = 0; vertex < 3; vertex++)
				{
					indexList[count++] = pMesh->GetPolygonVertex(polygon, vertex);
				}
			}
		}
		_polygonCountOfMaterial[i] = count / 3;

		g.pDevice->CreateIndexBuffer(sizeof(int) * _indexCount, 0,
			D3DFMT_INDEX32, D3DPOOL_MANAGED, &_pIndexBuffer[i], 0);
		DWORD *iCopy;
		_pIndexBuffer[i]->Lock(0, 0, (void**)&iCopy, 0);
		memcpy(iCopy, indexList, sizeof(int) * _indexCount);
		_pIndexBuffer[i]->Unlock();
		delete[] indexList;
	}

}



//描画処理
void Fbx::Draw()
{
	//移動行列
	D3DXMATRIX trans;
	D3DXMatrixTranslation(&trans, _position.x, _position.y, _position.z);

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(_rotate.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(_rotate.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(_rotate.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, _scale.x, _scale.y, _scale.z);

	//ワールド行列
	D3DXMATRIX world = scale * rotateZ * rotateX * rotateY * trans;

	//変換行列をセット
	g.pDevice->SetTransform(D3DTS_WORLD, &world);

	//頂点とインデックスバッファ
	g.pDevice->SetStreamSource(0, _pVertexBuffer, 0, sizeof(Vertex));


	//頂点情報の種類
	g.pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);



	//描画
	for (int i = 0; i < _materialCount; i++)
	{
		g.pDevice->SetIndices(_pIndexBuffer[i]);
		g.pDevice->SetMaterial(&_material[i]);
		g.pDevice->SetTexture(0, _pTexture[i]);
		g.pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, _vertexCount, 0, _polygonCountOfMaterial[i]);
	}

}


//////////////////////以下、アクセス関数////////////////////////////
void Fbx::SetPosition(D3DXVECTOR3 position)
{
	_position = position;
}

void Fbx::SetPosition(float x, float y, float z)
{
	_position.x = x;
	_position.y = y;
	_position.z = z;
}



void Fbx::SetRotate(D3DXVECTOR3 rotate)
{
	_rotate = rotate;
}

void Fbx::SetRotate(float x, float y, float z)
{
	_rotate.x = x;
	_rotate.y = y;
	_rotate.z = z;
}



void Fbx::SetScale(D3DXVECTOR3 scale)
{
	_scale = scale;
}


void Fbx::SetScale(float x, float y, float z)
{
	_scale.x = x;
	_scale.y = y;
	_scale.z = z;
}



//位置を取得
D3DXVECTOR3 Fbx::GetPosition()
{
	return _position;
}

//回転角度を取得
D3DXVECTOR3 Fbx::GetRotate()
{
	return _rotate;
}

//拡大率を取得
D3DXVECTOR3 Fbx::GetScale()
{
	return _scale;
}