﻿#pragma once
#include "Fbx.h"

class Bullet : public  MyGameEngine::Fbx
{
public:
	Bullet();
	~Bullet();
	HRESULT Init() override;
	BOOL Update() override;

	void Hit(Fbx* pTarget) override;
};

