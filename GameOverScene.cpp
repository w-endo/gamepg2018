﻿#include "GameOverScene.h"
#include "Camera.h"
#include "Light.h"
#include "Fbx.h"

GameOverScene::GameOverScene()
{
}


GameOverScene::~GameOverScene()
{
}

HRESULT GameOverScene::Init()
{
	_pNodes.push_back(new MyGameEngine::Light);
	_pNodes.push_back(new MyGameEngine::Camera);
	MyGameEngine::Fbx* logo = new MyGameEngine::Fbx;
	logo->Init("GameOver.fbx");
	_pNodes.push_back(logo);

	return S_OK;
}
