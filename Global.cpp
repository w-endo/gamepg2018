﻿#include "Global.h"
#include "Scene.h"

void Global::ChangeScene(MyGameEngine::Scene * newScene)
{
	SAFE_DELETE(pScene);
	pScene = newScene;
	pScene->Init();
	pScene->InitEach();
}
