﻿#include "TitleLogo.h"
#include "PlayScene.h"

TitleLogo::TitleLogo()
{
}

TitleLogo::~TitleLogo()
{
}

HRESULT TitleLogo::Init()
{
	return Fbx::Init("title.fbx");
}

BOOL TitleLogo::Update()
{
	if (g.pInput->IsKeyTap(DIK_SPACE))
	{
		g.ChangeScene(new PlayScene);
		return TRUE;
	}
	return FALSE;
}
