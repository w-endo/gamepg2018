﻿#include "PlayScene.h"
#include "GameOverScene.h"
#include "Camera.h"
#include "Light.h"
#include "Player.h"
#include "Enemy.h"


PlayScene::PlayScene()
{
}


PlayScene::~PlayScene()
{
}

HRESULT PlayScene::Init()
{
	//各オブジェクトの準備
	AddChild(new MyGameEngine::Light);
	AddChild(new MyGameEngine::Camera);
	AddChild(new Player);
	for (int i = 0; i < 3; i++)
	{
		AddChild(new Enemy);
	}
	return S_OK;

}

