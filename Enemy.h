﻿#pragma once
#include "Fbx.h"

class Enemy : public  MyGameEngine::Fbx
{
public:
	Enemy();
	~Enemy();
	HRESULT Init() override;
	BOOL Update() override;
};

