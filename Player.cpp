﻿#include "Player.h"
#include "Bullet.h"
#include "Scene.h"
#include "Enemy.h"
#include <typeinfo>
#include "GameOverScene.h"

using namespace MyGameEngine;


Player::Player()
{
}


Player::~Player()
{
}

HRESULT Player::Init()
{
	_pBaseBullet = new Bullet;
	_pBaseBullet->Init();
	return Fbx::Init("player.fbx");
}

BOOL Player::Update()
{
	if (g.pInput->IsKeyPush(DIK_LEFT))
	{
		_position.x -= 0.1f;
	}

	if (g.pInput->IsKeyPush(DIK_RIGHT))
	{
		_position.x += 0.1f;
	}

	if (g.pInput->IsKeyTap(DIK_SPACE))
	{
		Bullet* pBullet = new Bullet;
		memcpy(pBullet, _pBaseBullet, sizeof(Bullet));
		pBullet->SetPosition(_position);
		_parent->AddChild(pBullet);


	}
	return FALSE;
}


void Player::Hit(Fbx * pTarget)
{
	if (typeid(*pTarget) == typeid(Enemy))
	{
		//自分からpTargetに向かうベクトル
		D3DXVECTOR3 v = pTarget->GetPosition() - this->GetPosition();

		//そのベクトルの長さを測る
		float length = D3DXVec3Length(&v);

		if (length < 1)
		{
			g.ChangeScene(new GameOverScene);
		}
	}
}