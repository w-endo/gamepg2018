﻿#include "Node.h"
using namespace MyGameEngine;


Node::Node()
{
	_position = D3DXVECTOR3(0, 0, 0);
	_rotate = D3DXVECTOR3(0, 0, 0);
	_scale = D3DXVECTOR3(1, 1, 1);
	_isDead = FALSE;
}


Node::~Node()
{
}

