﻿#include "Camera.h"

using namespace MyGameEngine;

//コンストラクタ
Camera::Camera()
{
	_position = D3DXVECTOR3(0, 3, -5);
	_target = D3DXVECTOR3(0, 0, 0);
	_up = D3DXVECTOR3(0, 1, 0);

	_angle = 60;
	_aspect = (float)g.WINDOW_WIDTH / (float)g.WINDOW_HEIGHT;
	_nearClip = 0.3f;
	_farClip = 100.0f;

	D3DXMatrixIdentity(&_view);
	D3DXMatrixIdentity(&_proj);
}

//デストラクタ
Camera::~Camera()
{
}

//更新処理
BOOL Camera::Update()
{
	//ビュー行列
	D3DXMatrixLookAtLH(&_view, &_position, &_target, &_up);
	g.pDevice->SetTransform(D3DTS_VIEW, &_view);

	//プロジェクション行列
	D3DXMatrixPerspectiveFovLH(&_proj, D3DXToRadian(_angle), _aspect, _nearClip, _farClip);
	g.pDevice->SetTransform(D3DTS_PROJECTION, &_proj);

	return FALSE;
}


//////////////////以下セッター////////////////////////
void Camera::SetPosition(D3DXVECTOR3 position)
{
	_position = position;
}

void Camera::SetTarget(D3DXVECTOR3 target)
{
	_target = target;
}

void Camera::SetUp(D3DXVECTOR3 up)
{
	_up = up;
}


//////////////////以下ゲッター////////////////////////
D3DXVECTOR3 Camera::GetPosition()
{
	return _position;
}

D3DXVECTOR3 Camera::GetTarget()
{
	return _target;
}

D3DXVECTOR3 Camera::GetUp()
{
	return _up;
}
